package com.qualogy.action;

import com.qualogy.action.HelloWorldAction;
import com.qualogy.dto.HelloWorldFlowDto;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HelloWorldActionTest {
    private static String name = "test";

    private HelloWorldAction test = new HelloWorldAction();

    @Test
    public void test() {
        HelloWorldFlowDto flowDto = mock(HelloWorldFlowDto.class);
        when(flowDto.getName()).thenReturn(name);
        test.perform(flowDto);
        verify(flowDto).setHelloWorld(name+" says Hello");
    }
}
