package com.qualogy.resource;

import com.qualogy.controller.HelloWorldController;
import com.qualogy.dto.HelloWorldFlowDto;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import static com.qualogy.controller.AbstractController.Event.SAY_HELLO;

/**
 * A Jersey resource with a single @Path allows use of Spring's @Component
 */
@Path("/")
@Component
public class HelloWorldResource {

    @Resource
    private HelloWorldController helloWorldController;
    @Resource
    private HttpServletRequest httpServletRequest;

    /**
     * GET /hello
     * Get a name from a header
     * @return
     */
    @GET
    @Path("hello")
    public String sayHello() {
        HelloWorldFlowDto flowDto = new HelloWorldFlowDto();
        flowDto.setName(httpServletRequest.getHeader("name"));
        flowDto = helloWorldController.handleEvent(SAY_HELLO, flowDto);
        return flowDto.getHelloWorld();
    }

}
