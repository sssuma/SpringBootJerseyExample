package com.qualogy.resource;

import com.qualogy.controller.HelloWorldController;
import com.qualogy.data.PostRequest;
import com.qualogy.data.PostResponse;
import com.qualogy.dto.HelloWorldFlowDto;
import com.qualogy.service.MessageServiceAsInterface;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import static com.qualogy.controller.AbstractController.Event.GET_CONFIG;
import static com.qualogy.controller.AbstractController.Event.GET_CURRENT_DATETIME;
import static com.qualogy.controller.AbstractController.Event.SAY_HELLO;

/**
 * Since this class has multiple endpoints, Jersey will conflict with Spring's @Resource
 * Therefor all dependencies must be autowired with @Autowired
 */
@Slf4j
@Path("/")
public class KeepAliveResource {
    @Autowired
    private HelloWorldController helloWorldController;

    @Autowired
    private MessageServiceAsInterface messageServiceAsInterface;

    @GET
    @Path("keepalive")
    public String keepAliveComponent() {
        HelloWorldFlowDto flowDto = helloWorldController.handleEvent(GET_CURRENT_DATETIME, new HelloWorldFlowDto());
        return flowDto.getLocalDateTime().toString();
    }

    @GET
    @Path("keepAlive")
    public String keepAliveInterface() {
        return messageServiceAsInterface.getMessage();
    }

    @POST
    @Path("postAlive")
    public Response postAlive(@RequestBody PostRequest postRequest){
        HelloWorldFlowDto flowDto = new HelloWorldFlowDto();
        postRequest.extract(flowDto);
        helloWorldController.handleEvent(SAY_HELLO, flowDto);
        return Response.status(HttpStatus.SC_OK).entity(new PostResponse().inject(flowDto)).build();
    }

    @GET
    @Path("config/")
    public String getConfig(){
        HelloWorldFlowDto flowDto = new HelloWorldFlowDto();
        helloWorldController.handleEvent(GET_CONFIG,flowDto);
        return flowDto.getConfig();
    }
}
