package com.qualogy.action;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class LocalDateTimeAction implements Action<LocalDateTimeAction.KeepAlive> {

    @Override
    public void perform(KeepAlive flowDto) {
        LocalDateTime currentDateTime = LocalDateTime.now();
        flowDto.setLocalDateTime(currentDateTime);
    }

    public interface KeepAlive {
        void setLocalDateTime(LocalDateTime localDateTime);
    }
}
