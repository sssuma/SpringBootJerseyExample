package com.qualogy.action;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GetConfigAction implements Action<GetConfigAction.ConfigDto>{
    @Value("${helloworld.name}")
    private String helloWorld;

    @Override
    public void perform(ConfigDto flowDto) {
        flowDto.setConfig(helloWorld);
    }
    public interface ConfigDto {
        void setConfig(String value);
    }
}
