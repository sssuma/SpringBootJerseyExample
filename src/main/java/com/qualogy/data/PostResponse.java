package com.qualogy.data;

import com.qualogy.dto.HelloWorldFlowDto;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PostResponse implements ApiResponse<HelloWorldFlowDto> {
    private String message;

    @Override
    public PostResponse inject(HelloWorldFlowDto flowDto) {
        this.message = flowDto.getHelloWorld();
        return this;
    }
}
