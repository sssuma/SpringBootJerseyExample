package com.qualogy.data;

import com.qualogy.dto.AbstractFlowDto;

/**
 * This is a simple interface that expects an ApiRequest to define an extraction of its data into a FlowDto
 * @param <T>
 */
public interface ApiRequest<T extends AbstractFlowDto> {
    void extract(T t);
}
