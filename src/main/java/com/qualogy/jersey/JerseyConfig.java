package com.qualogy.jersey;

import com.qualogy.resource.HelloWorldResource;
import com.qualogy.resource.KeepAliveResource;
import com.qualogy.resource.PhilipsLampResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * The Jersey Configuration where we register all the REST resource classes
 */
@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(HelloWorldResource.class);
        register(KeepAliveResource.class);
        register(PhilipsLampResource.class);
    }
}
