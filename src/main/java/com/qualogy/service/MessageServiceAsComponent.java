package com.qualogy.service;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * This annotation creates a Bean of this class with name service(Class com.qualogy.service.Service)
 * Use (value="yourName") to specify a name
 * It can be called using @Resource or @Autowired, but only @Resource allows (name="yourName)s
 */
@Component(value="myService")
public class MessageServiceAsComponent {
    public String getMessage() {
            return LocalDateTime.now().toString();
    }
}
